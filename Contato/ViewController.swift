//
//  ViewController.swift
//  Contato
//
//  Created by COTEMIG on 11/08/22.
//

import UIKit

class Contato: Codable {
    var nome: String
    var email: String
    var endereco: String
    var telefone: String
    
    init(nome: String, email: String, endereco: String, telefone: String){
        self.nome = nome
        self.email = email
        self.endereco = endereco
        self.telefone = telefone
    }
}

class ViewController: UIViewController, UITableViewDataSource {
    
    var listaDeContatos:[Contato] = []
    
    var userDefauts = UserDefaults.standard
    let listaDeContatosKey = "ListaDeContatos"
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDeContatos.count
    }
	
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celula = tableview.dequeueReusableCell(withIdentifier: "MinhaCelula",for: indexPath) as! MyCell
        let contato = listaDeContatos[indexPath.row]
        
        celula.nome.text = contato.nome
        celula.email.text = contato.email
        celula.endereco.text = contato.endereco
        celula.telefone.text = contato.telefone
        
        return celula
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath)
            -> UISwipeActionsConfiguration? {
            let deleteAction = UIContextualAction(style: .destructive, title: nil) { (_, _, _) in
                
                let refreshAlert = UIAlertController(title: "Atenção", message: "Tem certeza que deseja excluir o contato?", preferredStyle: UIAlertController.Style.alert)

                refreshAlert.addAction(UIAlertAction(title: "Sim", style: .default, handler: { (action: UIAlertAction!) in
                  print("Handle Ok logic here")
                    self.excluirContato(index: indexPath.row)
                    self.navigationController?.popViewController(animated: true)
                  }))

                refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                  print("Handle Cancel Logic here")
                    self.navigationController?.popViewController(animated: true)
                  }))

                self.present(refreshAlert, animated: true, completion: nil)

            }
        
            let editAction = UIContextualAction(style: .destructive, title: nil) { (_, _, _) in
                self.performSegue(withIdentifier: "contato", sender: indexPath.row)
            }
        
            deleteAction.image = UIImage(systemName: "trash")
            deleteAction.backgroundColor = .systemRed
            editAction.image = UIImage(systemName: "pencil")
            editAction.backgroundColor = .systemGray
            let configuration = UISwipeActionsConfiguration(actions: [editAction, deleteAction])
            return configuration
    }
    
    @IBOutlet weak var tableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview.dataSource = self
        tableview.delegate = self
        
       
        
        do {
            if let listaData = userDefauts.value(forKey: listaDeContatosKey) as? Data {
                let listaContatos = try JSONDecoder().decode([Contato].self, from: listaData)
                listaDeContatos = listaContatos
            }
            tableview.reloadData()
        } catch {
            print("Erro ao converter os dados: \(error.localizedDescription)")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "abrirDetalhes" {
            let detalhesViewController = segue.destination as! DetalhesViewController
            let index = sender as! Int
            let contato = listaDeContatos[index]
            detalhesViewController.index = index
            detalhesViewController.contato = contato
            detalhesViewController.delegate = self
            detalhesViewController.contatoDelegate = self
        } else if segue.identifier == "contato" {
            let contatoViewController = segue.destination as! ContatoViewController
            contatoViewController.delegate = self
            
            let index = sender as? Int
            
            if index != nil {
                let contato = listaDeContatos[index!]
                contatoViewController.contato = contato
            }
        }
    }
    
    func salvarContatoLocal() {
        do {
            let dataLista = try JSONEncoder().encode(listaDeContatos)
            userDefauts.setValue(dataLista, forKey: listaDeContatosKey)
        } catch {
            print("Erro ao salvar dados na memória local: \(error.localizedDescription)")
        }

    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "abrirDetalhes", sender: indexPath.row)
    }
}

extension ViewController: ContatoViewControllerDelegate {
    func salvarNovoContato(contato: Contato) {
        listaDeContatos.append(contato)
        tableview.reloadData()
        salvarContatoLocal()
    }
    
    func editarContato() {
        tableview.reloadData()
        salvarContatoLocal()
    }
}

extension ViewController: DetalhesViewControllerDelegate{
    func excluirContato(index: Int) {
        
        self.listaDeContatos.remove(at: index)
        self.tableview.reloadData()
        self.salvarContatoLocal()
        
    }
}
