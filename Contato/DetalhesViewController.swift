//
//  DetalhesViewController.swift
//  Contato
//
//  Created by COTEMIG on 01/09/22.
//

import UIKit

protocol DetalhesViewControllerDelegate {
    func excluirContato(index: Int)
}

class DetalhesViewController: UIViewController {

    @IBOutlet weak var nomeLabel: UILabel!
    @IBOutlet weak var numeroLabel: UILabel!
    @IBOutlet weak var enderecoLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    public var index: Int?
    public var contato: Contato?
    public var delegate: DetalhesViewControllerDelegate?
    public var contatoDelegate: ContatoViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = contato?.nome
        
        nomeLabel.text = contato?.nome
        numeroLabel.text = contato?.telefone
        emailLabel.text = contato?.email
        enderecoLabel.text = contato?.endereco
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "editarContato" {
            let contatoViewController = segue.destination as? ContatoViewController
            contatoViewController?.contato = contato
            contatoViewController?.delegate = self
        }
    }
    
    @IBAction func excluirContatoTap(_ sender: Any) {
        
        let refreshAlert = UIAlertController(title: "Atenção", message: "Tem certeza que deseja excluir o contato?", preferredStyle: UIAlertController.Style.alert)

        refreshAlert.addAction(UIAlertAction(title: "Sim", style: .default, handler: { (action: UIAlertAction!) in
          print("Handle Ok logic here")
            self.delegate?.excluirContato(index: self.index!)
            self.navigationController?.popViewController(animated: true)
          }))

        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
          print("Handle Cancel Logic here")
            self.navigationController?.popViewController(animated: true)
          }))

        present(refreshAlert, animated: true, completion: nil)
    }
}

    extension DetalhesViewController: ContatoViewControllerDelegate {
        func salvarNovoContato(contato: Contato) {
        
    }
    
    func editarContato() {
        title = contato?.nome
        
        nomeLabel.text = contato?.nome
        numeroLabel.text = contato?.telefone
        emailLabel.text = contato?.email
        enderecoLabel.text = contato?.endereco
        
        contatoDelegate?.editarContato()
    }
}

